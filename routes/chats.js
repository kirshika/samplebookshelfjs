var Model = require('./../models/User');

/* Save a chat */
var saveChat = function (req, res) {
    new Model.Chat({
        userId: req.body.userId,
        message: req.body.message
    }).save()
        .then(function (chat) {
            res.json(chat);
        }).catch(function (error) {
        console.log(error);
        res.send('An error occured');
    });
};

/* Get all chats */
var getAllChats = function (req, res) {
    new Model.Chat().fetchAll()
        .then(function (chats) {
            res.json(chats);
        }).catch(function (error) {
        console.log(error);
        res.send('An error occured');
    });
};

/* Delete a chat */
var deleteChat = function (req, res) {
    var userId = req.params.id;
    new Model.Chat().where('id', userId)
        .destroy()
        .catch(function (error) {
            console.log(error);
            res.send('An error occured');
        });
};

/* Get a chat */
var getChat = function (req, res) {
    var userId = req.params.id;
    new Model.Chat().where('id', userId)
        .fetch()
        .then(function (chat) {
            res.json(chat);
        }).catch(function (error) {
        console.log(error);
        res.send('An error occured');
    });
};

/* Exports all methods */
module.exports = {
    saveChat: saveChat,
    getAllChats: getAllChats,
    deleteChat: deleteChat,
    getChat: getChat
};
