var bookshelf = require('./../config/db').bookshelf;

var User = bookshelf.Model.extend({
	tableName: 'users',
	chats:function(){
		return this.hasMany(Chat);
	}
});

var Chat = bookshelf.Model.extend({
    tableName: 'chats',
	users:function () {
		return this.belongsTo(User);
    }
});

module.exports = {
	User: User,
	Chat:Chat
};
