var user = require('./routes/users');
var chat = require('./routes/chats');
var index = require('./routes/index');

module.exports = function (app) {

	/* Index(main) route */
	app.get('/', index.index);

	/* User Routes */
	app.post('/users', user.saveUser);
	app.get('/users', user.getAllUsers);
	app.delete('/user/:id', user.deleteUser);
	app.get('/user/:id', user.getUser);

    /* Chat Routes */
    app.post('/chats', chat.saveChat);
    app.get('/chats', chat.getAllChats);
    app.delete('/chat/:id', chat.deleteChat);
    app.get('/chat/:id', chat.getChat);
};

