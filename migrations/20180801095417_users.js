'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('users', (table) => {
        table.increments('id').unsigned().primary();
        table.string('username');
        table.string('email').notNull();
        table.string('name');
        table.integer('age');
        table.string('location');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users');
};

