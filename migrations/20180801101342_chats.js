'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('chats', function(table) {
        table.increments('id').unsigned().primary();
        table.integer('userId').unsigned().references('users.id').onDelete('CASCADE').notNull();
        table.string('message');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('chats');
};
