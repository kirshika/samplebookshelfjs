module.exports = {

    development: {
        client: 'mysql',
        connection: {
            database: 'simple_bookshelf',
            user: 'root',
            password: 'root',
            host: '127.0.0.1'
        }
    }
};